# Summary

This repository provides a simple setup script to prepare a PC running one of the following supported OSs as a development ready PC

- Mac OS X Sierra and High Sierra (10.12 and 10.13) (recommended)
- Debian 8
- CentOS 7 (currently broken because of startup bug)
- Ubuntu 16.04+ (recommended)

## Requirements

To use the following script, you must have a Google Cloud sign-in with Editor access to
 to the `mycareplan-dev` Google Cloud project.  
 
 You must also have an account in BitBucket with access to the MyCarePlan team.
 
## Ubuntu OS Install

To install Ubuntu you will need:

- A 2GB or larger USB stick/flash drive or a DVD-ROM burner and blank disc
- Microsoft Windows XP or later 
         or 
  Mac OS 10.11 or Later 
         or
  Another linux distribution

Follow the "Easy ways to switch to Ubuntu" section on the website:

https://www.ubuntu.com/download/desktop

After installing make sure to enable auto updates on the system.

## Provides

- Homebrew (Mac OS only) (i.e. `brew`)
- Google Cloud SDK installation and configuration (i.e. `gcloud`)
- MySQL Workbench and client
- `docker` and `docker-compose`
- Google Chrome
- Kubernetes CLI (i.e. `kubectl`)
- Minikube and associated VM driver (i.e. `minikube`)
- Kubernetes Helm (i.e. `helm`)
- `wget` and `curl`
- PHP 7.0 (for debugging) (i.e. `php`)
- Git and Git Flow installation and configuration
- Clone and setup of MyCarePlan BitBucket git repositories ( Source located at `~/bb/` )
- Keychain (on OSs that don't support it natively) to save SSH key passwords
- Aliases for docker-compose common commands
- Bash 4, `bpkg` and Bash Completion helpers for all above supported utilities 

## Usage

Ensure the most recent system update is installed on your PC before running the setup script below.

You will be prompted to enter your password and will need to have `sudo` access to your PC.

When prompted to select a Google Cloud region/zone, select Y and option 11. (us-west1-b)

Run the following in a Terminal window and follow the prompts. 

You will be asked to create a password to secure your SSL key and enter it `4` times.

```bash
cd $HOME && \
    curl -Ss https://bitbucket.org/mycareplan/setup/raw/master/setup.sh > setup.sh || \
    wget https://bitbucket.org/mycareplan/setup/raw/master/setup.sh -O setup.sh && \
    chmod +x setup.sh && \
    ./setup.sh
```

## Errors

If you experience an error, the script can usually be re-run with:
```bash
~/setup.sh
```


## Notes

You may be prompted to accept a set of Certificate Authorities with a prompt to "Press Q to Quit"

You need to press Q to accept and continue the setup.