#!/usr/bin/env bash
for dir in $HOME/bb/*/
do
	if [[ $dir != *".git"* ]]; then
		cd $dir
		git checkout ${1:-master}
		git pull origin ${1:-master}
		cd ~/bb
    fi
done

