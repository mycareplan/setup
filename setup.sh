#!/usr/bin/env bash
# This setup script will configure a Mac OS, CentOS 7, Ubuntu 16
# or Debian Gnu Linux 8 PC for 
# development and testing in the Carecognetics environment

if [ `whoami` = "root" ]; then
  echo "Do not run this script as root"
  exit 1
fi

platform='unknown'
unamestr=`uname`
if [[ "$unamestr" == "Linux" ]]; then
   
  platform='linux'
  MK_DRIVER="kvm2"
    
elif [[ "$unamestr" == "Darwin" ]]; then
   
  platform="MacOS" 
  MK_DRIVER="hyperkit"
  
  if [ ! `command -v brew` ]; then
    echo "Installing Homebrew..."
    /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
  fi
else
  echo "Unsupported OS $unamestr";
  exit 3;
fi


if [ -z `echo "$*" | grep -e "--skip-install"` ]; then

# Homebrew specific
if [ `command -v brew` ]; then

  brew update
  brew upgrade
  brew install bash git wget bash-completion git-flow php@7.0 kubernetes-helm grep
  brew cask install docker virtualbox mysql-shell mysqlworkbench google-chrome google-cloud-sdk
  
  if [ ! -x /usr/local/bin/docker-machine-driver-hyperkit ]; then
    echo 'Installing Docker Machine Hyperkit driver'
    curl https://storage.googleapis.com/minikube/releases/latest/docker-machine-driver-hyperkit \
            > ~/docker-machine-driver-hyperkit \
            && chmod +x ~/docker-machine-driver-hyperkit \
            && sudo mv ~/docker-machine-driver-hyperkit /usr/local/bin/ \
            && sudo chown root:wheel /usr/local/bin/docker-machine-driver-hyperkit \
            && sudo chmod u+s /usr/local/bin/docker-machine-driver-hyperkit
  fi
  
  if [ -z `grep '\/usr\/local\/bin\/bash' /etc/shells` ]; then
    echo "Upgrading to Bash shell 4 from $(echo $BASH_VERSION)";
    sudo bash -c 'echo /usr/local/bin/bash >> /etc/shells'
    sudo chsh -s /usr/local/bin/bash $(whoami)
    exec /usr/local/bin/bash -c "$0 --skip-install $@"
  fi

  if [ -z "$(cat ~/.bash_profile | grep bash_completion)" ]; then
    sudo $SHELL -c 'echo "[ -f /usr/local/etc/bash_completion ] && \
        . /usr/local/etc/bash_completion" >> ~/.bash_profile'
  fi

  if [ ! -f "/Library/LaunchDaemons/com.user.lo0-loopback.plist" ]; then
    cat << EOF | sudo $SHELL
echo " <?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC -//Apple Computer//DTD PLIST 1.0//EN http://www.apple.com/DTDs/PropertyList-1.0.dtd >
<plist version="1.0">
<dict>
  <key>Label</key>
  <string>com.user.lo0-loopback</string>
  <key>ProgramArguments</key>
  <array>
    <string>/sbin/ifconfig</string>
    <string>lo0</string>
    <string>alias</string>
    <string>172.16.222.111</string>
  </array>
  <key>RunAtLoad</key> <true/>
  <key>Nice</key>
  <integer>10</integer>
  <key>KeepAlive</key>
  <false/>
  <key>AbandonProcessGroup</key>
  <true/>
  <key>StandardErrorPath</key>
  <string>/var/log/loopback-alias.log</string>
  <key>StandardOutPath</key>
  <string>/var/log/loopback-alias.log</string>
</dict>
</plist>" > /Library/LaunchDaemons/com.user.lo0-loopback.plist
chmod 0644 /Library/LaunchDaemons/com.user.lo0-loopback.plist
chown root:wheel /Library/LaunchDaemons/com.user.lo0-loopback.plist
launchctl load /Library/LaunchDaemons/com.user.lo0-loopback.plist
ifconfig lo0 alias 172.16.222.111
EOF
  fi


# Debian/Ubuntu specific
elif [ `command -v apt-get` ]; then

  export OS_RELEASE="$(lsb_release -si | tr '[:upper:]' '[:lower:]')"
  export OS_VERSION="$(lsb_release -cs)"
  export OS_VERSION_NUMBER="$(lsb_release -rs | tr -d '.')"
  
  if [ -z "$(sudo apt-key fingerprint mysql)" ]; then
    sudo apt-key adv --keyserver ha.pool.sks-keyservers.net --recv-keys 5072E1F5
  fi
  
  if [ ! -f "/etc/apt/sources.list.d/mysql.list" ]; then
    echo "deb http://repo.mysql.com/apt/${OS_RELEASE}/ ${OS_VERSION} mysql-5.7" | sudo tee --append /etc/apt/sources.list.d/mysql.list
    echo "deb http://repo.mysql.com/apt/${OS_RELEASE}/ ${OS_VERSION} mysql-tools" | sudo tee --append /etc/apt/sources.list.d/mysql.list
  fi
  
  if [[ "$OS_RELEASE" == "debian" ]]; then

    wget -q -O - https://www.dotdeb.org/dotdeb.gpg | sudo apt-key add -

    if [ ! -f "/etc/apt/sources.list.d/dotdeb.list" ]; then
      echo "deb http://packages.dotdeb.org ${OS_VERSION} all" | sudo tee --append /etc/apt/sources.list.d/dotdeb.list
      echo "deb-src http://packages.dotdeb.org ${OS_VERSION} all" | sudo tee --append /etc/apt/sources.list.d/dotdeb.list
    fi
  fi
  
  if [ -z "$(sudo apt-key fingerprint 7FAC5991)" ]; then
    wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | sudo apt-key add - 
  fi
  
  if [ -z "$(sudo apt-cache search google-chrome-stable)" ]; then
    sudo sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google-chrome.list'
  fi
  
  sudo apt-get update -y
  sudo apt-get upgrade -y --allow-unauthenticated
  sudo apt-get dist-upgrade -y --allow-unauthenticated
  sudo apt-get autoremove -y --allow-unauthenticated
  sudo apt-get install -y --allow-unauthenticated \
    curl git wget bash-completion git-flow google-chrome-stable \
    libvirt-bin qemu-kvm apt-transport-https mysql-workbench \
    mysql-client ca-certificates software-properties-common net-tools
  
  if [[ "$OS_RELEASE" == "debian" ]]; then
    sudo apt-get install -y --allow-unauthenticated \
        php7.0 php7.0-cli php7.0-opcache php7.0-mbstring php7.0-json \
        php7.0-mcrypt php7.0-mysql php7.0-redis php7.0-mcrypt
  elif [[ "$OS_RELEASE" == "ubuntu" ]]; then
    if [[ "${OS_VERSION_NUMBER}" -gt "1800" ]]; then
      sudo apt-get install -y --allow-unauthenticated \
        php php-cli php-opcache php-mbstring php-json  \
        php-mysql php-redis php-xdebug
    else
      sudo apt-get install -y --allow-unauthenticated \
        php php-pear php7.0-dev php7.0-zip php7.0-curl php7.0-gd \
        php7.0-mysql php7.0-mcrypt php7.0-xml libapache2-mod-php7.0 \
        php7.0-json php7.0-xml
    fi
  fi
  
  curl -fsSL https://download.docker.com/linux/${OS_RELEASE}/gpg | sudo apt-key add -
  
  
  if [ ! -f "/etc/apt/sources.list.d/docker.list" ]; then
    if [[ "${OS_VERSION}" == "bionic" ]]; then
        DOCKER_OS_VERSION="artful"  # Currently (2018-4-26) the 'bionic' release does not have a stable Docker CE build so
             # for now we're going to use the release from the prior OS (artful). This should be removed
             # when a stable release is available
    else
        DOCKER_OS_VERSION=$OS_VERSION;
    fi
    echo "deb [arch=amd64] https://download.docker.com/linux/${OS_RELEASE} ${DOCKER_OS_VERSION} stable" \
      | sudo tee --append /etc/apt/sources.list.d/docker.list
    
    sudo apt-get update -y

    echo "Waiting before Docker install"
    pause 15
  fi
  
  sudo apt-get install -y --allow-unauthenticated docker docker-ce
  
  if [ ! -x "/usr/local/bin/docker-machine-driver-kvm2" ]; then
    echo 'Installing Docker Machine KVM2 driver'
      
    curl https://storage.googleapis.com/minikube/releases/latest/docker-machine-driver-kvm2 \
      > ~/docker-machine-driver-kvm2 && \
      chmod +x ~/docker-machine-driver-kvm2 && \
      sudo mv ~/docker-machine-driver-kvm2 /usr/local/bin/
  fi
  
  sudo service docker start

  if [ -z "$(grep lo:0 /etc/network/interfaces | head -1)" ]; then
    cat << EOF | sudo $SHELL
echo "

auto lo:0
iface lo:0 inet static
address 172.16.222.111
netmask 255.0.0.0
" >> /etc/network/interfaces
service networking restart
EOF
  fi

# Centos/Fedora specific
elif [ `command -v yum` ]; then

  while [[ ! -z "$(ps -ef | grep [P]ackageKit)" ]]
  do
  	echo "PackageKit is currently working on updates. Please wait..."
  	sleep 60
  done
  
  REL_NUM=`cat /etc/*elease | grep VERSION_ID | cut -d'=' -f2 | tr -d '"'` 
  basearch=`uname -p`
  
  if [ ! -f "/etc/yum.repos.d/mysql.repo" ]; then
    cat << EOF | sudo $SHELL
REL_NUM=`cat /etc/*elease | grep VERSION_ID | cut -d'=' -f2 | tr -d '"'` \
basearch=`uname -p` 
echo "[mysql57-community]
name=MySQL 5.7 Community Server
baseurl=http://repo.mysql.com/yum/mysql-5.7-community/el/${REL_NUM}/${basearch}/
enabled=1
gpgcheck=1
gpgkey=https://raw.githubusercontent.com/example42/puppet-yum/master/files/rpm-gpg/RPM-GPG-KEY-mysql

[mysql-tools-community]
name=MySQL 5.7 Community Server
baseurl=http://repo.mysql.com/yum/mysql-tools-community/el/${REL_NUM}/${basearch}/
enabled=1
gpgcheck=1
gpgkey=https://raw.githubusercontent.com/example42/puppet-yum/master/files/rpm-gpg/RPM-GPG-KEY-mysql
" > "/etc/yum.repos.d/mysql.repo"
EOF
    sudo rpm --import https://raw.githubusercontent.com/example42/puppet-yum/master/files/rpm-gpg/RPM-GPG-KEY-mysql
  fi
  
  if [ ! -f "/etc/yum.repos.d/google-chrome.repo" ]; then
    cat << EOF | sudo $SHELL 
basearch=`uname -p`
echo "[google-chrome]
name=google-chrome
baseurl=http://dl.google.com/linux/chrome/rpm/stable/${basearch}
enabled=1
gpgcheck=1
gpgkey=https://dl.google.com/linux/linux_signing_key.pub" > "/etc/yum.repos.d/google-chrome.repo"
EOF
    sudo rpm --import https://dl.google.com/linux/linux_signing_key.pub
  fi
  
  sudo rpm --import http://wiki.psychotic.ninja/RPM-GPG-KEY-psychotic
  sudo rpm -ivh "http://packages.psychotic.ninja/${REL_NUM}/base/${PNAME}/RPMS/psychotic-release-1.0.0-1.el${REL_NUM}.psychotic.noarch.rpm"
  
  if [ ! -f "/etc/yum.repos.d/docker-ce.repo" ]; then
    sudo $SHELL -c 'curl "https://download.docker.com/linux/centos/docker-ce.repo" | sed -e "s/\$basearch/$(uname -p)/" > /etc/yum.repos.d/docker-ce.repo'
  fi

  echo "Updating Yum. (NOTE:  If yum gets frozen with an error related to "
  echo "    locked files.  You must wait until your PC finishes all automatic"
  echo "    updates before running this script)"
  
  curl 'https://setup.ius.io/' | sudo bash

  sudo yum-config-manager  --save --setopt=base.exclude=kernel kernel-devel kernel-PAE-*;
  sudo yum-config-manager  --save --setopt=updates.exclude=kernel kernel-devel kernel-PAE-*;
  sudo yum-config-manager  --save --setopt=centosplus.includepkgs=kernel *;
  
  sudo yum-config-manager --enable psychotic
  sudo yum-config-manager --enable google-chrome
  sudo yum-config-manager --enable mysql57-community
  sudo yum-config-manager --enable mysql-tools-community
  sudo yum-config-manager --enable docker-ce-stable
  
  sudo yum update -y
  sudo yum install -y \
    git wget bash-completion gitflow libvirt-daemon-kvm \
    qemu-kvm google-chrome-stable \
    yum-utils mysql-workbench-community mysql-community-client \
    mod_php70u php70u-common php70u-cli php70u-redis php70u-mysqlnd \
    php70u-xml php70u-json php70u-opcache php70u-mcrypt php70u-mbstring \
    device-mapper-persistent-data lvm2 docker-ce keychain kernel-plus
  
  if [ ! -x "/usr/local/bin/docker-machine-driver-kvm2" ]; then
    echo 'Installing Docker Machine KVM2 driver'
  
    curl https://storage.googleapis.com/minikube/releases/latest/docker-machine-driver-kvm2 \
      > ~/docker-machine-driver-kvm2 && \
      chmod +x ~/docker-machine-driver-kvm2 && \
      sudo mv ~/docker-machine-driver-kvm2 /usr/local/bin/
  fi

  sudo service docker start
fi

fi

if [[ -d '/usr/local/mysql/bin' && -z "$(grep -s mysql $HOME/.bash_profile | head -1)" ]]; then
  echo 'export PATH=/usr/local/mysql/bin:$PATH' >> ~/.bash_profile
fi


if [ ! `command -v helm` ]; then
  echo "Installing Kubernetes Helm"
  curl https://raw.githubusercontent.com/kubernetes/helm/master/scripts/get | bash
fi

# latest version
DC_VERSION=`git ls-remote https://github.com/docker/compose | grep refs/tags | \
  perl -nle 'print $& if m{[0-9]+\.[0-9]+\.[0-9]+}' | tr '.' ' ' | \
  awk '{ printf "%03d %03d %03d\n", $1, $2, $3 }' | sort | tail -n 1 | \
  awk '{ printf "%d.%d.%d", $1, $2, $3 }'`
# fixed version
# DC_VERSION="1.21.0"
DC_INS_VERSION=""
if [ `command -v docker-compose` ]; then
	DC_INS_VERSION="$(docker-compose -version | perl -nle 'print $& if m{[0-9]+\.[0-9]+\.[0-9]+}')"
fi

if [[ "$DC_VERSION" != "$DC_INS_VERSION" ]]; then
  echo "Installing Docker Compose version ${DC_VERSION}"
  sudo curl -L https://github.com/docker/compose/releases/download/${DC_VERSION}/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose
  sudo chmod +x /usr/local/bin/docker-compose
fi

if [[ -f "$HOME/.bash_profile" ]] && [[ -z "$(grep -s docker\-compose $HOME/.bash_profile | head -1)" && -z "$(grep -s .bash_aliases $HOME/.bash_profile | head -1)" ]]; then
  echo "alias build='docker-compose build'
alias up='docker-compose up'
alias stop='docker-compose stop'
alias down='docker-compose down'" >> ~/.bash_profile
elif [[ ! -f "$HOME/.bash_aliases" || -z "$(grep -s docker\-compose $HOME/.bash_aliases | head -1)" ]]; then
  echo "alias build='docker-compose build'
alias up='docker-compose up'
alias stop='docker-compose stop'
alias down='docker-compose down'" >> ~/.bash_aliases
fi

if [ -d "/etc/bash_completion.d" ]; then
  BC_HOME="/etc/bash_completion.d/"
elif [ -d "/usr/local/etc/bash_completion.d" ]; then
  BC_HOME="/usr/local/etc/bash_completion.d/"
else 
  echo "Unable to install bash completion entries as it is not supported"
fi

if [ ! -z "$BC_HOME" -a ! -f "$BC_HOME/git-flow-completion.bash" ]; then
  echo "Install git flow bash completion..."
  curl "https://raw.githubusercontent.com/bobthecow/git-flow-completion/master/git-flow-completion.bash" > ~/git-flow-completion.bash
  sudo mv ~/git-flow-completion.bash $BC_HOME
  source $BC_HOME/git-flow-completion.bash
fi

if [ ! `command -v bpkg` ]; then
  echo "Installing bpkg..."
  curl -Lo- "https://raw.githubusercontent.com/bpkg/bpkg/master/setup.sh" | sudo bash
fi

#export CLOUDSDK_CORE_DISABLE_PROMPTS=1
#
#if [ ! `command -v gcloud` ]; then
#
#  if [ -d "$HOME/google-cloud-sdk" ]; then
#  	echo "Restart shell and run setup again.  If you have seen this message more than once, restart your computer then run ./setup.sh --skip-install"
#  	exit 5
#  fi
#
#  echo "Installing Google Cloud SDK..."
#
#  curl https://sdk.cloud.google.com | bash
#
#  echo "# The next line updates PATH for the Google Cloud SDK.
#if [ -f '/home/$(whoami)/google-cloud-sdk/path.bash.inc' ]; then source '/home/$(whoami)/google-cloud-sdk/path.bash.inc'; fi
#
## The next line enables shell command completion for gcloud.
#if [ -f '/home/$(whoami)/google-cloud-sdk/completion.bash.inc' ]; then source '/home/$(whoami)/google-cloud-sdk/completion.bash.inc'; fi
#" >> ~/.bashrc
#
#  echo "Run setup again using the following commands:"
#  echo "----------------------------------------------------------"
#  echo ""
#  if [ ! -z "$(command -v newgrp)" -a ! -z "$(sudo $SHELL -c 'command -v usermod')" ]; then
#  	if [ ! -z "$(grep 'docker' /etc/group | head -1)" ]; then
#  	    echo "sudo usermod -a -G docker $(whoami)"
#    	echo "newgrp docker"
#    fi
#    if [ ! -z "$(grep 'www-data' /etc/group | head -1)" ]; then
#    	echo "sudo usermod -a -G www-data $(whoami)"
#    	echo "newgrp www-data"
#    fi
#    if [ ! -z "$(grep 'libvirtd' /etc/group | head -1)" ]; then
#      echo "sudo usermod -a -G libvirtd $(whoami)"
#      echo "newgrp libvirtd"
#    elif [ ! -z "$(grep 'libvirt' /etc/group | head -1)" ]; then
#      echo "sudo usermod -a -G libvirt $(whoami)"
#      echo "newgrp libvirt"
#    fi
#  fi
#
#  if [ -z "$(echo '$*' | grep -e '--skip-install')" -a ! -z "$(grep 'docker' /etc/group)" ]; then
#  	echo "$0 --skip-install $*"
#  else
#  	echo "$0 $*"
#  fi
#  exec -l $SHELL
#fi

DOCKER_GROUP=`id -nG "$USER" | grep "docker"`
if [ ! -z "$(grep 'docker' /etc/group)" -a -z "${DOCKER_GROUP}" ]; then
    echo "User needs to be added to docker group"
    echo "Copy/Paste and run the following commands in this terminal:"
    echo "----------------------------------------------------------"
    echo ""
    if [ ! -z "$(grep 'www-data' /etc/group | head -1)" ]; then
    	echo "sudo usermod -a -G www-data $(whoami)"
    	echo "newgrp www-data"
    fi
    if [ ! -z "$(grep 'libvirtd' /etc/group | head -1)" ]; then
      echo "sudo usermod -a -G libvirtd $(whoami)"
      echo "newgrp libvirtd"
    elif [ ! -z "$(grep 'libvirt' /etc/group | head -1)" ]; then
      echo "sudo usermod -a -G libvirt $(whoami)"
      echo "newgrp libvirt"
    fi
    echo "sudo usermod -a -G docker $(whoami)"
    echo "newgrp docker"
    if [ -z "$(echo '$*' | grep -e '--skip-install')" ]; then
      echo "$0 --skip-install $*"
    else
      echo "$0 $*"
    fi
    echo ""
    exit 1
fi
#
#if [ ! `command -v kubectl` ]; then
#  gcloud components install kubectl beta
#fi
#
#if [ -z "$(gcloud config get-value account)" ]; then
#  export CLOUDSDK_CORE_DISABLE_PROMPTS=0
#  gcloud beta init --project mycareplan-dev
#  export CLOUDSDK_CORE_DISABLE_PROMPTS=1
#  gcloud auth configure-docker
#fi
#
#if [ ! -f ~/.config/gcloud/application_default_credentials.json ]; then
#  gcloud auth application-default login
#fi
#
#export CLOUDSDK_CORE_DISABLE_PROMPTS=0
#
#MK_VERSION="v0.26.1"
#
#MK_VERSION=`git ls-remote https://github.com/kubernetes/minikube | grep refs/tags | \
#  perl -nle 'print $& if m{[0-9]+\.[0-9]+\.[0-9]+}' | tr '.' ' ' | \
#  awk '{ printf "%03d %03d %03d\n", $1, $2, $3 }' | sort | tail -n 1 | \
#  awk '{ printf "v%d.%d.%d", $1, $2, $3 }'`
#MK_INS_VERS=""
#
#if [ `command -v minikube` ]; then
#  MK_INS_VERS="$(minikube version | perl -nle 'print $& if m{v[0-9]+\.[0-9]+\.[0-9]+}')"
#fi
#
#if [[ "$MK_INS_VERS" != "$MK_VERSION" ]]; then
#  echo "Installing Minikube version ${MK_VERSION}..."
#
#  if [[ "$platform" == "MacOS" ]]; then
#    curl -Lo minikube https://storage.googleapis.com/minikube/releases/${MK_VERSION}/minikube-darwin-amd64 \
#      && chmod +x minikube \
#      && sudo mv minikube /usr/local/bin/
#  else
#    curl -Lo minikube https://storage.googleapis.com/minikube/releases/${MK_VERSION}/minikube-linux-amd64 \
#      && chmod +x minikube \
#      && sudo mv minikube /usr/local/bin/
#  fi
#
#  minikube config set vm-driver $MK_DRIVER
#fi

if [ ! -f "$HOME/.ssh/id_rsa" ]; then
  
  SSH_PASS=""
  VERIFY_PASS=""
  while [[ ${#SSH_PASS} -lt 8 || "$VERIFY_PASS" != "$SSH_PASS" ]]
  do
    if [[ ! -z "$SSH_PASS" && ${#SSH_PASS} -lt 8 ]]; then
      echo "Pass phrase must be at least 8 characters"
  	elif [[ ! -z "$VERIFY_PASS" ]]; then
  	  echo "Pass phrases did not match"
  	fi
    read -p "Enter a new SSH pass phrase of at least 8 characters: " SSH_PASS
    
    if [[ ${#SSH_PASS} -gt 7 ]]; then
		read -p "Re-enter the pass phrase: " VERIFY_PASS
    fi
  done
  
  echo "Generating new SSH key in $HOME/.ssh/id_rsa"
  
  ssh-keygen -q -t rsa -N $SSH_PASS -f ~/.ssh/id_rsa 
  
  if [ "$platform" = "MacOS" ]; then
    ssh-add -K
  else
    ssh-add "$HOME/.ssh/id_rsa"
  fi

fi

if [ -z "$(git config --global --get user.email)" -o -z "$(git config --global --get user.name)" ]; then
  echo "-------- Configuring Git ------------"
#  EMAIL="$(gcloud config get-value account)"
  GIT_NAME=""
  GIT_EMAIL=""
  while [[ ${#GIT_EMAIL} -lt 3 ]]
  do
    read -p "Enter your email for Git: " GIT_EMAIL
#    if [ -z "$GIT_EMAIL" ]; then
#      GIT_EMAIL="$EMAIL"
#    fi
  done
  
  while [[ ${#GIT_NAME} -lt 3 ]]
  do
    read -p "Enter your name (e.g. Jim Bean) for Git: " GIT_NAME
  done
  git config --global user.email $GIT_EMAIL
  git config --global user.name $GIT_NAME

fi

git config --global merge.ours.driver true

if [[ "$platform" == "MacOS" ]]; then
    git config --global credential.helper osxkeychain
else
    git config --global credential.helper 'cache --timeout 86400'
fi

echo "----- Add the following key to your BitBucket/Github SSH authorized keys ------"
echo "------ See https://bitbucket.org/account/user/YOUR_USER/ssh-keys/ -----------"
echo ""
cat "$HOME/.ssh/id_rsa.pub"
echo ""
echo "------------------------------------------------------------------------"
echo "Press enter after you have added the key to continue..."
read

mkdir -p ~/bb
chmod 775 ~/bb
cd ~/bb

if [ ! -d "$HOME/bb/git-template" ]; then
  echo "Cloning git template repo..."
  git clone git@bitbucket.org:mycareplan/git-template.git
  git config --global init.templateDir "$HOME/bb/git-template"
fi

  git config --global branch.autosetuprebase always

REPOS=('setup' 'helm-charts' 'careadmin')

for i in "${REPOS[@]}"
do
  if [ -d $i ]; then
    echo "Repo already exists at $HOME/bb/$i"
  else   
    echo "Cloning $REPO Repository..."
    git clone "git@bitbucket.org:mycareplan/${i}.git"
    if [[ "${i}" == "careadmin" ]]; then
        cd careadmin
        docker-compose build
        cd ..
    elif [[ "${i}" == "setup" ]]; then
      if [ -f "$HOME/bb/setup/setup.sh" -a ! -f "$HOME/setup.sh" ]; then
        rm ~/setup.sh
        ln -s $HOME/bb/setup/setup.sh ~/setup.sh
      fi
      if [ -f "$HOME/bb/setup/pull_all.sh" -a ! -f "$HOME/bb/pull_all.sh" ]; then
        rm ~/setup.sh
        ln -s $HOME/bb/setup/pull_all.sh ~/bb/pull_all.sh
      fi
    fi
  fi
done

echo "############################################################"
echo "###############        Setup complete        ###############"
echo "############################################################"
echo "############################################################"
echo "###############                              ###############"
echo "###############       Press Enter to         ###############"
echo "###############   Restart you computer NOW   ###############"
echo "###############                              ###############"
echo "############################################################"
read
sudo shutdown -r now
